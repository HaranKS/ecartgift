package com.example.ecart.dto;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CartRequestDto {

	private Integer productId;
	private Integer customerId;
	private Integer quantity;
	private String category;
	private List<UserDto> userDto;
	public Integer getProductId() {
		return productId;
	}
	public void setProductId(Integer productId) {
		this.productId = productId;
	}
	public Integer getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}
	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public List<UserDto> getUserDto() {
		return userDto;
	}
	public void setUserDto(List<UserDto> userDto) {
		this.userDto = userDto;
	}
	
	
}
