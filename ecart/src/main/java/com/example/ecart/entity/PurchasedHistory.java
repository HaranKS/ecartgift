package com.example.ecart.entity;

import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
public class PurchasedHistory {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer purchasedHistoryId;
	private LocalDate purchasedDate;
	private Integer cartId;
	public Integer getPurchasedHistoryId() {
		return purchasedHistoryId;
	}
	public void setPurchasedHistoryId(Integer purchasedHistoryId) {
		this.purchasedHistoryId = purchasedHistoryId;
	}
	public LocalDate getPurchasedDate() {
		return purchasedDate;
	}
	public void setPurchasedDate(LocalDate purchasedDate) {
		this.purchasedDate = purchasedDate;
	}
	public Integer getCartId() {
		return cartId;
	}
	public void setCartId(Integer cartId) {
		this.cartId = cartId;
	}

}
