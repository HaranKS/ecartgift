package com.example.ecart.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.example.ecart.entity.Product;

@Service
public interface ProductService {

	List<Product> getListOfProducts();

}
