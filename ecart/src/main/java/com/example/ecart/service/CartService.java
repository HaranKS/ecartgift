package com.example.ecart.service;

import org.springframework.stereotype.Service;

import com.example.ecart.dto.CartRequestDto;
import com.example.ecart.dto.CartUpdateDto;
import com.example.ecart.dto.ResponseDto;
import com.example.ecart.exception.CartException;

@Service
public interface CartService {
	
	public ResponseDto confirmOrder(Integer cartId) throws CartException;

	public ResponseDto addProducts(CartRequestDto cartRequestDto) throws CartException;

	public ResponseDto updateCart(CartUpdateDto cartUpdateDto) throws CartException;

}
