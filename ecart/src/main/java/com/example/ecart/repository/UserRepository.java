package com.example.ecart.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.ecart.entity.User;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {
	Optional<List<User>> findByCartId(Integer cartId);

	User findByUserId(Integer userId);

}
