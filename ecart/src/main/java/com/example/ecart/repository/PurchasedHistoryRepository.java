package com.example.ecart.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.ecart.entity.PurchasedHistory;

@Repository
public interface PurchasedHistoryRepository extends JpaRepository<PurchasedHistory, Integer> {

}
